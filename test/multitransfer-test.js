const { expect } = require("chai");

describe("Multitransfer", function () {
  it("Should allow to transfer multiple ERC721", async function () {
    const [owner, addr1, addr2] = await ethers.getSigners();

    const DemoERC721 = await ethers.getContractFactory(
      "ERC721PresetMinterPauserAutoId"
    );
    const demoERC721 = await DemoERC721.deploy(
      "Multitransfer Demo",
      "MTD",
      "a"
    );

    await demoERC721.deployed();

    mintTxs = [];
    mintTxs.push(await demoERC721.mint(addr1.address));
    mintTxs.push(await demoERC721.mint(addr1.address));

    await Promise.all(mintTxs.map((tx) => tx.wait()));

    expect(await demoERC721.balanceOf(addr1.address)).to.equal(2);

    const Multitransfer = await ethers.getContractFactory(
      "MultitransferERC721"
    );
    const multitransfer = await Multitransfer.deploy();
    await multitransfer.deployed();

    const aproveTx = await expect(
      demoERC721.connect(addr1).setApprovalForAll(multitransfer.address, true)
    )
      .to.emit(demoERC721, "ApprovalForAll")
      .withArgs(addr1.address, multitransfer.address, true);

    const transferTx = await multitransfer
      .connect(addr1)
      .transfer(demoERC721.address, addr2.address, [0, 1]);

    // wait until the transaction is mined
    await transferTx.wait();

    expect(await demoERC721.balanceOf(addr2.address)).to.equal(2);
  });
  it("Should not allow to transfer tokens from not owner", async function () {
    const [owner, addr1, addr2] = await ethers.getSigners();

    const DemoERC721 = await ethers.getContractFactory(
      "ERC721PresetMinterPauserAutoId"
    );
    const demoERC721 = await DemoERC721.deploy(
      "Multitransfer Demo",
      "MTD",
      "a"
    );

    await demoERC721.deployed();

    mintTxs = [];
    mintTxs.push(await demoERC721.mint(addr1.address));
    mintTxs.push(await demoERC721.mint(addr1.address));

    await Promise.all(mintTxs.map((tx) => tx.wait()));

    expect(await demoERC721.balanceOf(addr1.address)).to.equal(2);

    const Multitransfer = await ethers.getContractFactory(
      "MultitransferERC721"
    );
    const multitransfer = await Multitransfer.deploy();
    await multitransfer.deployed();

    const aproveTx = await expect(
      demoERC721.connect(addr1).setApprovalForAll(multitransfer.address, true)
    )
      .to.emit(demoERC721, "ApprovalForAll")
      .withArgs(addr1.address, multitransfer.address, true);

    expect(
      multitransfer
        .connect(addr2)
        .transfer(demoERC721.address, addr2.address, [0, 1])
    ).to.be.revertedWith("ERC721: transfer of token that is not own");
  });
});
