// SPDX-License-Identifier: MIT

pragma solidity ^0.8.0;

import "@openzeppelin/contracts/token/ERC721/IERC721.sol";

contract MultitransferERC721 {
    function transfer(
        address erc721Address,
        address recipient,
        uint256[] calldata tokenIds
    ) public {
        IERC721 erc721 = IERC721(erc721Address);

        uint256 tokenIdsCount = tokenIds.length;

        for (uint256 i = 0; i < tokenIdsCount; i++) {
            erc721.safeTransferFrom(msg.sender, recipient, tokenIds[i]);
        }
    }
}
